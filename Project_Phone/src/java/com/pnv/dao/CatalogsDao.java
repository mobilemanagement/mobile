/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Catalogs;
import java.util.List;

/**
 *
 * @author khochA
 */
public interface CatalogsDao {
    public void saveOrUpdate(Catalogs catalogs);

    public void delete(Catalogs catalogs);

    public List<Catalogs> findAll();

    public Catalogs findByCatalogsId(int catalogId);

    public Catalogs findByCatalogsName(String catalogsName);
}
