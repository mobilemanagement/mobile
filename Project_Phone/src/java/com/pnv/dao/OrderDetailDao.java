/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Orderdetails;
import com.pnv.models.Orders;
import java.util.List;

/**
 *
 * @author khochA
 */
public interface OrderDetailDao {
    public void saveOrUpdate(Orderdetails orderdetails);

    public void delete(Orderdetails orderdetails);

    public List<Orderdetails> findAll();

    public Orderdetails findByOrderdetailsId(int orderDetailId);
}
