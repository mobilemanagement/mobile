/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Customers;
import java.util.List;

/**
 *
 * @author khochA
 */
public interface CustomerDao {
    public void saveOrUpdate(Customers customer);

    public void delete(Customers customer);

    public List<Customers> findAll();

    public Customers findByCustomersId(int customerId);

    public Customers findByCustomersName(String customerName);
}
