/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Customers;
import com.pnv.utils.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

/**
 *
 * @author khochA
 */
@Service
public class CustomerImplement implements CustomerDao{
    @Override
    public void saveOrUpdate(Customers customer) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(customer);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(Customers customer) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(customer);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

    @Override
    public List<Customers> findAll() {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Customers> customerList = session.createQuery("from Customers").list();
        session.close();
        return customerList;
    }

    @Override
    public Customers findByCustomersId(int customerId) {
        Customers customer = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            customer = (Customers) session.get(Customers.class, customerId);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return customer;
    }

    @Override
    public Customers findByCustomersName(String customerName) {
        Customers customer = null;
        String strQuery = "from Customers WHERE customerName = :customerName ";
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter("customerName", customerName);
        customer = (Customers) query.uniqueResult();
        session.close();
        return customer;
    }
}
