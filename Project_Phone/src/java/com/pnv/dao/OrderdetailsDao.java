/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Orderdetails;
import java.util.List;

/**
 *
 * @author khochA
 */
public interface OrderdetailsDao {
    public void saveOrUpdate(Orderdetails orderdetails);

    public void delete(Orderdetails orderdetails);

    public List<Orderdetails> findAll();

    public Orderdetails findByorderDetailId(int orderDetailId);

 
}
