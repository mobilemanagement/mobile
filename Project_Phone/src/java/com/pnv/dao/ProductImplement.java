/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Product;
import com.pnv.utils.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

/**
 *
 * @author khochA
 */
@Service
public class ProductImplement implements ProductDao{

    
    @Override
    public void saveOrUpdate(Product product) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(product);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(Product product) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(product);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

    @Override
    public List<Product> findAll() {
       
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Product> productList = session.createQuery("from Product").list();
        session.close();
        return productList;
    }

    @Override
    public Product findByProductId(int id) {
        Product product = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            product = (Product) session.get(Product.class, id);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return product;
    }

    @Override
    public Product findByProductName(String productName) {
        Product product = null;
        String strQuery = "from Departments WHERE productName = :proName ";
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter("proName", productName);
        product = (Product) query.uniqueResult();
        session.close();
        return product;
    }
    
}
