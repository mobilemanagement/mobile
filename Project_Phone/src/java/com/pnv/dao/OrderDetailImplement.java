/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Orderdetails;
import com.pnv.utils.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

/**
 *
 * @author khochA
 */
@Service
public class OrderDetailImplement implements OrderDetailDao{

    @Override
    public void saveOrUpdate(Orderdetails orderdetails) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(orderdetails);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        } 
    }

    @Override
    public void delete(Orderdetails orderdetails) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(orderdetails);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

    @Override
    public List<Orderdetails> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Orderdetails> ordersDetailList = session.createQuery("from Orderdetails").list();
        session.close();
        return ordersDetailList;
    }

    @Override
    public Orderdetails findByOrderdetailsId(int orderDetailId) {
        Orderdetails orderdetails = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            orderdetails = (Orderdetails) session.get(Orderdetails.class, orderDetailId);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return orderdetails;
    }
}
    

