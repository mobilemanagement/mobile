/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Orders;
import java.util.List;

/**
 *
 * @author khochA
 */
public interface OrdersDao {
    
    public void saveOrUpdate(Orders orders);

    public void delete(Orders catalogs);

    public List<Orders> findAll();

    public Orders findByOrderId(int orderId);
  
}
