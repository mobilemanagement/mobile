/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Admin;
import java.util.List;


/**
 *
 * @author hungnq
 */
public interface AdminDao {
    public List<Admin> findAll();

    public Admin findByUsername(String username);

    public Admin findByPassword(String password_);
}

