/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Catalogs;
import com.pnv.utils.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

/**
 *
 * @author khochA
 */
@Service
public class CatalogsImplement implements CatalogsDao{


    
    @Override
    public void saveOrUpdate(Catalogs catalogs) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(catalogs);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(Catalogs catalogs) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(catalogs);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

    @Override
    public List<Catalogs> findAll() {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Catalogs> catalogsList = session.createQuery("from Catalogs").list();
        session.close();
        return catalogsList;
    }

    @Override
    public Catalogs findByCatalogsId(int catalogId) {
        Catalogs catalogs = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            catalogs = (Catalogs) session.get(Catalogs.class, catalogId);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return catalogs;
    }

    @Override
    public Catalogs findByCatalogsName(String catalogsName) {
        Catalogs catalogs = null;
        String strQuery = "from Catalogs WHERE catalogName = :cataName ";
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter("cataName", catalogsName);
        catalogs = (Catalogs) query.uniqueResult();
        session.close();
        return catalogs;
    }
    
}
