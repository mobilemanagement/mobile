/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Product;
import java.util.List;

/**
 *
 * @author khochA
 */
public interface ProductDao {
    public void saveOrUpdate(Product product);

    public void delete(Product product);

    public List<Product> findAll();

    public Product findByProductId(int id);

    public Product findByProductName(String productName);
}
