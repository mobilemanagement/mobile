/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Admin;
import com.pnv.utils.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;



  
@Service
public class AdminDaoImplemanet implements AdminDao {

    @Override
    public List<Admin> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Admin> admin = session.createQuery("from Admin").list();
        session.close();
        return admin;
    }

    @Override
    public Admin findByUsername(String username) {
        Admin admin = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            admin = (Admin) session.get(Admin.class, username);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return admin;
    }

    @Override
    public Admin findByPassword(String password_) {
        Admin admin = null;
        String strQuery = "from Admin WHERE catalogName = :cataName ";
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter("password_", password_);
        admin = (Admin) query.uniqueResult();
        session.close();
        return admin;
    }

//    @SuppressWarnings("unchecked")
//    public Admin findByusername(String username) {
//
//        List<Admin> admin = new ArrayList<Admin>();
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try {
//            admin = session.createQuery("from Admin where username=?")
//                    .setParameter(0, username).list();
//        } catch (HibernateException hb) {
//            System.err.println("error" + hb);
//        } finally {
//            session.close();
//        }
//
//
//        if (admin.size() > 0) {
//            return admin.get(0);
//        } else {
//            return null;
//        }

    }
