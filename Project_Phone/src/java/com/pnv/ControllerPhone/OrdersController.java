/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.ControllerPhone;

import com.pnv.dao.OrdersDao;
import com.pnv.models.Orders;
import com.pnv.utils.Constant;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
/**
 *
 * @author khochA
 */
@Controller
@RequestMapping(value = "/orders")
public class OrdersController {
    
    @Autowired
    private OrdersDao ordersDao;
    
    public OrdersController(){
    }
    @RequestMapping(method = RequestMethod.GET)
    public String viewOrderPage(ModelMap map) {
        /**
         * Get all Product
         */
        List<Orders> order_list = ordersDao.findAll();
        map.put("order_list", order_list);
        return "orders";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditOrdersPage(@RequestParam(value = "orderId", required = true) int orderId, ModelMap map) {

        Orders orderForm = ordersDao.findByOrderId(orderId);
        map.addAttribute("orderForm", orderForm);
        return "addOrders";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Orders orderForm = new Orders();
        map.addAttribute("orderForm", orderForm);
        return "addOrders";

    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("orderForm") Orders orderForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("orderForm", orderForm);
            return "addOrders";
        }
        ordersDao.saveOrUpdate(orderForm);

        /**
         * Get all titles
         */
        List<Orders> order_list = ordersDao.findAll();
        map.put("order_list", order_list);
        map.addAttribute("add_success", "ok");

        return "orders";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "orderId", required = true) int orderId, ModelMap map) {

        ordersDao.delete(ordersDao.findByOrderId(orderId));

        return Constant.REDIRECT + "/orders";

    }
}
