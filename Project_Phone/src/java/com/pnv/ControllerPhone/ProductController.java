/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.ControllerPhone;

import com.pnv.dao.ProductDao;
import com.pnv.models.Product;
import com.pnv.utils.Constant;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
/**
 *
 * @author khochA
 */
@Controller
@RequestMapping(value = "/product")
public class ProductController {
    
    @Autowired
    private ProductDao productDao;
    
    public ProductController(){
    }
    @RequestMapping(method = RequestMethod.GET)
    public String viewProductPage(ModelMap map) {
        /**
         * Get all Product
         */
        List<Product> product_list = productDao.findAll();
        map.put("product_list", product_list);
        return "product";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditProductPage(@RequestParam(value = "productId", required = true) int productId, ModelMap map) {

        Product productForm = productDao.findByProductId(productId);
        map.addAttribute("productForm", productForm);
        return "addProduct";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Product productForm = new Product();
        map.addAttribute("productForm", productForm);
        return "addProduct";

    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("productForm") Product productForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("productForm", productForm);
            return "addProduct";
        }
        productDao.saveOrUpdate(productForm);

        /**
         * Get all titles
         */
        List<Product> product_list = productDao.findAll();
        map.put("product_list", product_list);
        map.addAttribute("add_success", "Insert is successful!");

        return "product";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "productId", required = true) int productId, ModelMap map) {

        productDao.delete(productDao.findByProductId(productId));

        return Constant.REDIRECT + "/product";

    }
}
