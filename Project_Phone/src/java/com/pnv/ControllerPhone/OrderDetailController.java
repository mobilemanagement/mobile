/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.ControllerPhone;

import com.pnv.dao.OrderDetailDao;
import com.pnv.models.Orderdetails;
import com.pnv.utils.Constant;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author khochA
 */
@Controller
@RequestMapping(value = "/orderDetail")
public class OrderDetailController {
        
    @Autowired
    private OrderDetailDao ordersdetailDao;   
    
    public OrderDetailController(){
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String viewOrderPage(ModelMap map) {

        /**
         * Get all catalogs
         */
        List<Orderdetails> orderDetail_list = ordersdetailDao.findAll();
        map.put("orderDetail_list", orderDetail_list);
        return "orderDetail";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditOrdersDetailPage(@RequestParam(value = "orderDetailId", required = true) int orderDetailId, ModelMap map) {

        Orderdetails orderDetailForm = ordersdetailDao.findByOrderdetailsId(orderDetailId);
        map.addAttribute("orderDetailForm", orderDetailForm);
        return "addOrderDetail";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Orderdetails orderDetailForm = new Orderdetails();
        map.addAttribute("orderDetailForm", orderDetailForm);
        return "addOrderDetail";

    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("orderDetailForm") Orderdetails orderDetailForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("orderDetailForm", orderDetailForm);
            return "addOrderDetail";
        }
        ordersdetailDao.saveOrUpdate(orderDetailForm);

        /**
         * Get all titles
         */
        List<Orderdetails> orderDetail_list = ordersdetailDao.findAll();
        map.put("orderDetail_list", orderDetail_list);
        map.addAttribute("add_success", "ok");

        return "orderDetail";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteOrdersDetail(@RequestParam(value = "orderDetailId", required = true) int orderDetailId, ModelMap map) {

        ordersdetailDao.delete(ordersdetailDao.findByOrderdetailsId(orderDetailId));

        return Constant.REDIRECT + "/orderDetail";

    }
}
