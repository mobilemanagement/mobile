/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.ControllerPhone;

import com.pnv.dao.CatalogsDao;
import com.pnv.models.Catalogs;
import com.pnv.utils.Constant;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author khochA
 */
@Controller
@RequestMapping(value = "/catalogs")
public class CatalogsController {
        
    @Autowired
    private CatalogsDao catalogsDao;   
    
    public CatalogsController(){
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String viewDepartmentPage(ModelMap map) {

        /**
         * Get all catalogs
         */
        List<Catalogs> catalogs_list = catalogsDao.findAll();
        map.put("catalogs_list", catalogs_list);
        return "catalogs";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditCatalogsPage(@RequestParam(value = "catalogId", required = true) int catalogId, ModelMap map) {

        Catalogs catalogsForm = catalogsDao.findByCatalogsId(catalogId);
        map.addAttribute("catalogsForm", catalogsForm);
        return "addCatalogs";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Catalogs catalogsForm = new Catalogs();
        map.addAttribute("catalogsForm", catalogsForm);
        return "addCatalogs";

    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("catalogsForm") Catalogs catalogsForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("catalogsForm", catalogsForm);
            return "addCatalogs";
        }
        catalogsDao.saveOrUpdate(catalogsForm);

        /**
         * Get all titles
         */
        List<Catalogs> catalogs_list = catalogsDao.findAll();
        map.put("catalogs_list", catalogs_list);
        map.addAttribute("add_success", "ok");

        return "catalogs";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "catalogId", required = true) int catalogId, ModelMap map) {

        catalogsDao.delete(catalogsDao.findByCatalogsId(catalogId));

        return Constant.REDIRECT + "/catalogs";

    }
}
