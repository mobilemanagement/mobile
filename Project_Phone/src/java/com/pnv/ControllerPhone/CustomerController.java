/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.ControllerPhone;


import com.pnv.dao.CustomerDao;
import com.pnv.models.Customers;
import com.pnv.utils.Constant;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author khochA
 */
@Controller
@RequestMapping(value = "/customer")
public class CustomerController {
    
    @Autowired
    private CustomerDao customerDao;   
    
    public CustomerController(){
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String viewCustomersPage(ModelMap map) {

        /**
         * Get all customer
         */
        List<Customers> customer_list = customerDao.findAll();
        map.put("customer_list", customer_list);
        return "customer";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditCustomersPage(@RequestParam(value = "customerId", required = true) int customerId, ModelMap map) {

        Customers customerForm = customerDao.findByCustomersId(customerId);
        map.addAttribute("customerForm", customerForm);
        return "addCustomer";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Customers customerForm = new Customers();
        map.addAttribute("customerForm", customerForm);
        return "addCustomer";

    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("customerForm") Customers customerForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("customerForm", customerForm);
            return "addCustomer";
        }
        customerDao.saveOrUpdate(customerForm);

        /**
         * Get all titles
         */
        List<Customers> customer_list = customerDao.findAll();
        map.put("customer_list", customer_list);
        map.addAttribute("add_success", "ok");

        return "customer";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteCustomer(@RequestParam(value = "customerId", required = true) int customerId, ModelMap map) {

        customerDao.delete(customerDao.findByCustomersId(customerId));

        return Constant.REDIRECT + "/customer";

    }
}
