/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.ControllerPhone;

import com.pnv.dao.AdminDao;
import com.pnv.dao.ProductDao;
import com.pnv.dao.ProductImplement;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.pnv.models.Admin;
import com.pnv.models.Product;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 *
 * @author hungnq
 */
@Controller
@RequestMapping(value = "/Admin")
public class LoginController {

    @Autowired
    AdminDao adminDao;
    private ProductImplement productDao;
    @RequestMapping(method = RequestMethod.POST)
    public String Admin(@ModelAttribute(value = "tk") Admin tk, ModelMap mm, HttpSession session) {
        Admin admin = adminDao.findByUsername(tk.getUsername());
        if (admin == null || !(tk.getPassword().equals(admin.getPassword()))) {
            mm.put("message", "User or password not correct");
            return "Admin";
        }
        productDao = new ProductImplement();
        List<Product> product_list = productDao.findAll();
        mm.put("product_list", product_list);
        session.setAttribute("username", tk.getUsername());
        return "index";
    }

}