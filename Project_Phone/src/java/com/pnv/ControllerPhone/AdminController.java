/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.ControllerPhone;

import com.pnv.dao.CatalogsDao;
import com.pnv.dao.ProductDao;
import com.pnv.models.Catalogs;
import com.pnv.models.Product;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author khochA
 */
@Controller
public class AdminController {
    @Autowired
    private ProductDao productDao;
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String viewWelcomePage(ModelMap map) {
        List<Product> product_list = productDao.findAll();
        map.put("product_list", product_list);
        return "index";
    }
        @RequestMapping(value = "/home", method = RequestMethod.GET)
        public String viewHome(ModelMap map) {
        List<Product> product_list = productDao.findAll();
        map.put("product_list", product_list);
        return "index";
    }
        
        @RequestMapping(value = "/login", method = RequestMethod.GET)
        public String viewLogin(ModelMap map) {
        List<Product> product_list = productDao.findAll();
        map.put("product_list", product_list);
        return "Admin";
    }

        @RequestMapping(value = "/administrator", method = RequestMethod.GET)
        public String viewAdministrator(ModelMap map) {
        List<Product> product_list = productDao.findAll();
        map.put("product_list", product_list);
        return "product";
    }
        
}
