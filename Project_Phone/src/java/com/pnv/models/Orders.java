package com.pnv.models;
// Generated May 11, 2015 3:34:53 PM by Hibernate Tools 3.2.1.GA


import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Orders generated by hbm2java
 */
@Entity
@Table(name="orders"
    ,catalog="phonemanagement"
)
public class Orders  implements java.io.Serializable {


     private Integer orderId;
     private Customers customers;
     private Integer quantity;
     private BigDecimal totalPrice;
     private List<Orderdetails> orderdetailses;

    public Orders() {
    }

    public Orders(Customers customers, Integer quantity, BigDecimal totalPrice, List<Orderdetails> orderdetailses) {
       this.customers = customers;
       this.quantity = quantity;
       this.totalPrice = totalPrice;
       this.orderdetailses = orderdetailses;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)
    
    @Column(name="orderId", unique=true, nullable=false)
    public Integer getOrderId() {
        return this.orderId;
    }
    
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }
    @JsonIgnore
@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="customerId")
    public Customers getCustomers() {
        return this.customers;
    }
    
    public void setCustomers(Customers customers) {
        this.customers = customers;
    }
    
    @Column(name="quantity")
    public Integer getQuantity() {
        return this.quantity;
    }
    
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    
    @Column(name="totalPrice", precision=10)
    public BigDecimal getTotalPrice() {
        return this.totalPrice;
    }
    
    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }
    @JsonIgnore
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="orders")
    public List<Orderdetails> getOrderdetailses() {
        return this.orderdetailses;
    }
    
    public void setOrderdetailses(List<Orderdetails> orderdetailses) {
        this.orderdetailses = orderdetailses;
    }




}


