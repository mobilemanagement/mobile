/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.controller;

import com.pnv.dao.OrderDetailDao;
import com.pnv.dao.OrdersDao;
import com.pnv.models.Orderdetails;
import com.pnv.models.Orders;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author khochA
 */
@Controller
@RequestMapping(value = "api/orderDetail")
public class OrderDetailApiController {
    @Autowired
    private OrderDetailDao orderDetailDao;
    @RequestMapping(method = RequestMethod.GET)
    public  @ResponseBody List<Orderdetails> viewOrdersDetailPage() {
        /**
         * Get all titles
         */
        List<Orderdetails> ordersDetail_list = orderDetailDao.findAll();
        return ordersDetail_list;
    }
    
    @RequestMapping(value = "{orderDetailId}", method = RequestMethod.GET)
    public  @ResponseBody Orderdetails getOrdresByID(@PathVariable(value = "orderDetailId") Integer orderDetailId) {
        
//        Employees emp = new Employees();
//        emp.setEmpName("ddd");
//        emp.setAddress("ssss");
//        List<Employees> emp_lst = new ArrayList<Employees>();
        Orderdetails orde = orderDetailDao.findByOrderdetailsId(orderDetailId);
        
        
       // dep.setEmployees(emp_lst);
        return orderDetailDao.findByOrderdetailsId(orderDetailId);
    }
}
