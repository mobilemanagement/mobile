/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.controller;

import com.pnv.dao.ProductDao;
import com.pnv.models.Product;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author khochA
 */
@Controller
@RequestMapping(value = "api/content")
public class ContentApiController {
        @Autowired
    private ProductDao productDao;
    
    @RequestMapping(method = RequestMethod.GET)
    public  @ResponseBody List<Product> viewDepartmentPage() {
        /**
         * Get all titles
         */
        List<Product> product_list = productDao.findAll();
        return product_list;
    }
    
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public  @ResponseBody Product getDepartmentByID(@PathVariable(value = "id") Integer id) {
        
//        Employees emp = new Employees();
//        emp.setEmpName("ddd");
//        emp.setAddress("ssss");
//        List<Employees> emp_lst = new ArrayList<Employees>();
        Product pro = productDao.findByProductId(id);
       // dep.setEmployees(emp_lst);
        return productDao.findByProductId(id);
    }
}

