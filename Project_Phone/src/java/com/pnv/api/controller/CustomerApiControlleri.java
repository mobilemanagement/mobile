/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.controller;

import com.pnv.dao.CustomerDao;
import com.pnv.models.Customers;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author khochA
 */

@Controller
@RequestMapping(value = "api/customer")
public class CustomerApiControlleri {
    
    @Autowired
    private CustomerDao customerDao;
    @RequestMapping(method = RequestMethod.GET)
    public  @ResponseBody List<Customers> viewCustomersPage() {
        /**
         * Get all titles
         */
        List<Customers> customer_list = customerDao.findAll();
        return customer_list;
    }
    
    @RequestMapping(value = "{customerId}", method = RequestMethod.GET)
    public  @ResponseBody Customers getCustomersByID(@PathVariable(value = "customerId") Integer customerId) {
        
//        Employees emp = new Employees();
//        emp.setEmpName("ddd");
//        emp.setAddress("ssss");
//        List<Employees> emp_lst = new ArrayList<Employees>();
        Customers cus = customerDao.findByCustomersId(customerId);
       // dep.setEmployees(emp_lst);
        return customerDao.findByCustomersId(customerId);
    }
}
