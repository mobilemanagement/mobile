/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.controller;

import com.pnv.dao.OrdersDao;
import com.pnv.models.Orders;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author khochA
 */

@Controller
@RequestMapping(value = "api/orders")
public class OrderApiController {
    @Autowired
    private OrdersDao ordersDao;
    @RequestMapping(method = RequestMethod.GET)
    public  @ResponseBody List<Orders> viewOrdersPage() {
        /**
         * Get all titles
         */
        List<Orders> orders_list = ordersDao.findAll();
        return orders_list;
    }
    
    @RequestMapping(value = "{orderId}", method = RequestMethod.GET)
    public  @ResponseBody Orders getOrdresByID(@PathVariable(value = "orderId") Integer orderId) {
        
//        Employees emp = new Employees();
//        emp.setEmpName("ddd");
//        emp.setAddress("ssss");
//        List<Employees> emp_lst = new ArrayList<Employees>();
        Orders ord = ordersDao.findByOrderId(orderId);
        
        
       // dep.setEmployees(emp_lst);
        return ordersDao.findByOrderId(orderId);
    }
        
}
