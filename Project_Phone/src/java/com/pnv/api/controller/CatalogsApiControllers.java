/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.controller;

import com.pnv.dao.CatalogsDao;
import com.pnv.models.Catalogs;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author khochA
 */
@Controller
@RequestMapping(value = "api/catalogs")
public class CatalogsApiControllers {
    
    @Autowired
    private CatalogsDao catalogsDao;
    @RequestMapping(method = RequestMethod.GET)
    public  @ResponseBody List<Catalogs> viewDepartmentPage() {
        /**
         * Get all titles
         */
        List<Catalogs> catalogs_list = catalogsDao.findAll();
        return catalogs_list;
    }
    
    @RequestMapping(value = "{catalogId}", method = RequestMethod.GET)
    public  @ResponseBody Catalogs getDepartmentByID(@PathVariable(value = "catalogId") Integer catalogId) {
        
//        Employees emp = new Employees();
//        emp.setEmpName("ddd");
//        emp.setAddress("ssss");
//        List<Employees> emp_lst = new ArrayList<Employees>();
        Catalogs cata = catalogsDao.findByCatalogsId(catalogId);
       // dep.setEmployees(emp_lst);
        return catalogsDao.findByCatalogsId(catalogId);
    }   
}
