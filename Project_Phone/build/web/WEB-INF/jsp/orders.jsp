<%@include file="header.jsp" %>

        <h1>Orders Management</h1>
        <h3>${add_success}</h3>


        <table border="1" style="margin-left: 10px">
            <tr>
                <th>ID</th>
                <th>Customer's Name</th>
                <th>Quantity</th>
                <th>Total Price</th>
                <th>Action</th>
            </tr>

                <c:forEach items="${order_list}" var="orders">  
                <tr>  
                    <td><c:out value="${orders.orderId}"/></td>  
                    <td><c:out value="${orders.customers.customerName}"/></td>  
                    <td><c:out value="${orders.quantity}"/></td>
                    <td><c:out value="${orders.totalPrice}"/></td>
                    <td align="center"><a href="<%=request.getContextPath()%>/orders/edit?orderId=${orders.orderId}">Edit</a> | <a href="<%=request.getContextPath()%>/orders/delete?orderId=${orders.orderId}">Delete</a></td>  
                </tr>  
            </c:forEach> 
    </table><br/>        
        <a href="<%=request.getContextPath()%>/orders/add" class="button">Add New</a><br/>
        
        <%@include file="footer.jsp" %>