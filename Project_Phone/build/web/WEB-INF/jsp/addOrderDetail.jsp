<%@include file="header.jsp" %>

        <h1>Add New Order Details</h1>

    <div align="center">
    
    <table border="0" width="90%">
        <form:form action="add" modelAttribute="orderDetailForm" method="POST" >
            <form:hidden path="orderDetailId" />
      
            <tr>
                <td>Product Id: </td>
                <td><form:input path="product.productId" size="30"/></td>
                <td><form:errors path="product.productId" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Price: </td>
                <td><form:input path="price" size="30"/></td>
                <td><form:errors path="price" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Quantity: </td>
                <td><form:input path="quantity" size="30"/></td>
                <td><form:errors path="quantity" cssClass="error"/></td>
            </tr>

                    <tr>
                        <td><a href="<%=request.getContextPath()%>/orderDetail" class="button">Back</a></td>
                        <td align="center"><input  class="buttonForm" type="submit" value="submit"/></td>
                        <td></td>
                    </tr>
                    </form:form>
                </table>
            </div>
        <%@include file="footer.jsp" %>
