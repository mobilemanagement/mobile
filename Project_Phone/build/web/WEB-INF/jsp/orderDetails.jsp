<%-- 
    Document   : customer
    Created on : May 14, 2015, 9:56:24 AM
    Author     : khochA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Customer's Management</h1>
        <h3>${add_success}</h3>
        <a href="<%=request.getContextPath()%>/orderDetails/add" class="button">Add New</a>

        <table border="1">
            <tr>
                <th>ID</th>
                <th>OrderID</th>
                <th>ProductID</th>
                <th>Price</th>
                <th>Quantity</th>
               
                <c:forEach items="${orderDetail_list}" var="orderDetails">  
                <tr>  
                    <td><c:out value="${orderDetails.orderDetailId}"/></td>  
                    <td><c:out value="${orderDetails.orderId}"/></td> 
                    <td><c:out value="${orderDetails.productId}"/></td>
                    <td><c:out value="${orderDetails.price}"/></td>  
                    <td><c:out value="${orderDetails.quantity}"/></td>
                    <td align="center"><a href="<%=request.getContextPath()%>/orderDetails/edit?customerId=${orderDetails.orderDetailId}">Edit</a> | <a href="<%=request.getContextPath()%>/orderDetails/delete?orderDetailId=${orderDetails.orderDetailId}">Delete</a></td>  
                </tr>  
            </c:forEach> 
        </tr>
        </table>
    </body>
</html>
