<%@include file="header.jsp" %>


        <h1>Customer's Management</h1>
        <h3>${add_success}</h3>


        <table border="1" style="margin-left: 10px">
            <tr>
                <th>ID</th>
                <th>Customers Name</th>
                <th>Phone Number</th>
                <th>Address</th>
                <th>Email</th>
                <th>Action</th>
                <c:forEach items="${customer_list}" var="customer">  
                <tr>  
                    <td><c:out value="${customer.customerId}"/></td>  
                    <td><c:out value="${customer.customerName}"/></td> 
                    <td><c:out value="${customer.phoneNumber}"/></td>
                    <td><c:out value="${customer.address}"/></td>  
                    <td><c:out value="${customer.email}"/></td>
                    <td align="center"><a href="<%=request.getContextPath()%>/customer/edit?customerId=${customer.customerId}">Edit</a> | <a href="<%=request.getContextPath()%>/customer/delete?customerId=${customer.customerId}">Delete</a></td>  
                </tr>  
            </c:forEach> 
        </tr>
        </table><br/>
                <a href="<%=request.getContextPath()%>/customer/add" class="button">Add New</a><br/>
        <%@include file="footer.jsp" %>
