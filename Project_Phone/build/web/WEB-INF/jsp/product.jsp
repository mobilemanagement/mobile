<%@include file="header.jsp" %>
        <h1>Product Management</h1>
        <h3>${add_success}</h3>
        <table border="1" style="margin-left: 10px">
            <tr>
                <th>ID</th>
                <th>Catalogs Name</th>
                <th>Product Name</th>
                <th>Price</th>
                <th>Image</th>
                <th>Description</th>
                <th>Action</th> 
                        </tr>

                <c:forEach items="${product_list}" var="product">  
                <tr>  
                    <td><c:out value="${product.productId}"/></td>  
                    <td><c:out value="${product.catalogs.catalogName}"/></td>
                    <td><c:out value="${product.productName}"/></td>
                    <td><c:out value="${product.priceProduct}"/></td>
                    <td  align="center"><img src="<c:url value="/resources/images/${product.imageLink}"/>" > </td>
                    <td><c:out value="${product.description}"/></td>          
                    <td align="center"><a href="<%=request.getContextPath()%>/product/edit?productId=${product.productId}">Edit</a> | <a href="<%=request.getContextPath()%>/product/delete?productId=${product.productId}">Delete</a></td>  
                </tr>  
            </c:forEach>
        </table><br/>
                <a href="<%=request.getContextPath()%>/product/add" class="button">Add New</a><br/>
<%@include file="footer.jsp" %>
