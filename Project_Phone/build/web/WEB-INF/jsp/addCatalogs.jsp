<%@include file="header.jsp" %>

        <h1>Add New Catalogs</h1>

<div align="center">
    
    <table border="0" width="90%">
        <form:form action="add" modelAttribute="catalogsForm" method="POST" >
            <form:hidden path="catalogId" />
            <tr>
                <td>Catalogs Name: </td>
                <td><form:input path="catalogName" size="30"/></td>
                <td><form:errors path="catalogName" cssClass="error"/></td>
            </tr>
           <tr>
                <td>Quantity: </td>
                <td><form:input path="quantity" size="30"/></td>
                <td><form:errors path="quantity" cssClass="error"/></td>
            </tr>

                    <tr>
                        <td></td>
                        <td align="center"><input  class="buttonForm" type="submit" value="submit"/></td>
                        <td></td>
                    </tr>
                    </form:form>
                </table>
            </div>
        <a href="<%=request.getContextPath()%>/catalogs" class="button">Back</a><br/>
    <%@include file="footer.jsp" %>
