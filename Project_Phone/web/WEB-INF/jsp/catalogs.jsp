<%@include file="header.jsp" %>

        <h1>Catalogs Management</h1>
        <h3>${add_success}</h3>


        <table border="1" style="margin-left: 10px">
            <tr>
                <th>ID</th>
                <th>Catalogs Name</th>
                <th>Quantity</th>
                <th>Action</th>
                <c:forEach items="${catalogs_list}" var="catalogs">  
                <tr>  
                    <td><c:out value="${catalogs.catalogId}"/></td>  
                    <td><c:out value="${catalogs.catalogName}"/></td>  
                    <td><c:out value="${catalogs.quantity}"/></td>
                    <td align="center"><a href="<%=request.getContextPath()%>/catalogs/edit?catalogId=${catalogs.catalogId}">Edit</a> | <a href="<%=request.getContextPath()%>/catalogs/delete?catalogId=${catalogs.catalogId}">Delete</a></td>  
                </tr>  
            </c:forEach> 
        </tr>
        </table><br/>
                <a href="<%=request.getContextPath()%>/catalogs/add" class="button">Add New</a><br/>
        <%@include file="footer.jsp" %>
