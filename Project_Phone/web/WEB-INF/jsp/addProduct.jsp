<%@include file="header.jsp" %>

        <h1>Add New Product</h1>

    <div align="center">
    
    <table border="0" width="90%">
        <form:form action="add" modelAttribute="productForm" method="POST" >
            <form:hidden path="productId" />
            <tr>
                <td align="left" width="20%">Catalog Id: </td>
                <td align="left" width="40%"><form:input path="catalogs.catalogId" size="30"/></td>
                <td align="left"><form:errors path="catalogs.catalogId" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Product Name: </td>
                <td><form:input path="productName" size="30"/></td>
                <td><form:errors path="productName" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Price: </td>
                <td><form:input path="priceProduct" size="30"/></td>
                <td><form:errors path="priceProduct" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Image Link: </td>
                <td><form:input path="imageLink" size="30"/></td>
                <td><form:errors path="imageLink" cssClass="error"/></td>
            </tr>
           <tr>
                <td>Description: </td>
                <td><form:input path="description" size="30"/></td>
                <td><form:errors path="description" cssClass="error"/></td>
            </tr>

                    <tr>
                        <td></td>
                        <td align="center"><input  class="buttonForm" type="submit" value="submit"/></td>
                        <td></td>
                    </tr>
                    </form:form>
                </table>

            </div>
                <a href="<%=request.getContextPath()%>/product" class="button">Back</a><br/>
<%@include file="footer.jsp" %>
