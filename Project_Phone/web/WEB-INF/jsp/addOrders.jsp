<%@include file="header.jsp" %>

        <h1>Add New Order</h1>

    <div align="center">
    
    <table border="0" width="90%">
        <form:form action="add" modelAttribute="orderForm" method="POST" >
            <form:hidden path="orderId" />
            <tr>
                <td>Customer Id: </td>
                <td><form:input path="customers.customerId" size="30"/></td>
                <td><form:errors path="customers.customerId" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Quantity: </td>
                <td><form:input path="quantity" size="30"/></td>
                <td><form:errors path="quantity" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Total Price: </td>
                <td><form:input path="totalPrice" size="30"/></td>
                <td><form:errors path="totalPrice" cssClass="error"/></td>
            </tr>
                    <tr>
                        <td><a href="<%=request.getContextPath()%>/orders" class="button">Back</a></td>
                        <td align="center"><input  class="buttonForm" type="submit" value="submit"/></td>
                        <td></td>
                    </tr>
                    </form:form>
                </table>
            </div>
   <%@include file="footer.jsp" %>

