
<%@include file="header.jsp" %>
        <h1>Order Detail Management</h1>
        <h3>${add_success}</h3>


        <table border="1" style="margin-left: 10px">
            <tr>
                <th>ID</th>
                <th>Product Name </th> 
                <th>Price</th>
                <th>Quantity</th>
                <th>Action</th>
                        </tr>

                <c:forEach items="${orderDetail_list}" var="orderDetail">  
                <tr>  
                    <td><c:out value="${orderDetail.orderDetailId}"/></td>  
                    <td><c:out value="${orderDetail.product.productName}"/></td>  
                    <td><c:out value="${orderDetail.price}"/></td>
                    <td><c:out value="${orderDetail.quantity}"/></td>
                    <td align="center"><a href="<%=request.getContextPath()%>/orderDetail/edit?orderDetailId=${orderDetail.orderDetailId}">Edit</a> | <a href="<%=request.getContextPath()%>/orderDetail/delete?orderDetailId=${orderDetail.orderDetailId}">Delete</a></td>  
                </tr>  
            </c:forEach> 
        </table><br/>
                <a href="<%=request.getContextPath()%>/orderDetail/add" class="button">Add New</a><br/>
        <%@include file="footer.jsp" %>
