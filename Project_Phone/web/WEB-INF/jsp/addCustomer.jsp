<%@include file="header.jsp" %>

        <h1>Add New Customers</h1>

<div align="center">
    
    <table border="0" width="90%">
        <form:form action="add" modelAttribute="customerForm" method="POST" >
            <form:hidden path="customerId" />
            <tr>
                <td>Customer's Name: </td>
                <td><form:input path="customerName" size="30"/></td>
                <td><form:errors path="customerName" cssClass="error"/></td>
            </tr>
           <tr>
                <td>Phone Number: </td>
                <td><form:input path="phoneNumber" size="30"/></td>
                <td><form:errors path="phoneNumber" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Address: </td>
                <td><form:input path="address" size="30"/></td>
                <td><form:errors path="address" cssClass="error"/></td>
            </tr>
           <tr>
                <td>Email: </td>
                <td><form:input path="Email" size="30"/></td>
                <td><form:errors path="Email" cssClass="error"/></td>
            </tr>

                    <tr>
                        <td><a href="<%=request.getContextPath()%>/customer" class="button">Back</a></td>
                        <td align="center"><input  class="buttonForm" type="submit" value="submit"/></td>
                        <td></td>
                    </tr>
                    </form:form>
                </table>
            </div>
        <%@include file="footer.jsp" %>
