package com.example.administrator.jsonphone;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;


public class MainActivity extends ActionBarActivity {
    TextView catalogsid;
    TextView catalogname;
    TextView quantity;
    public TextView findTextView(int id)
    {
        return (TextView) findViewById(id);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addControlAndEvents();
    }
    public void addControlAndEvents()
    {
        catalogsid = findTextView(R.id.txtcatalogid);
        catalogname = findTextView(R.id.txtcatalogname);
        quantity = findTextView(R.id.txtquantity);
    }
    @Override
    protected void onResume()
    {
        // TODO Auto-generated method stub
        super.onResume();
        //http://192.168.103.84:8084/Project_phone/api/catalogs
        //http://192.168.103.84:8084/Project_phone/api/catalogs
//        new MyJsonTask().execute("http://graph.facebook.com/kyduyenhoahau");
        new MyJsonTask().execute("http://192.168.103.84:8084/Project_phone/api/catalogs");
}
    //lop xu li da tien trinh
    public class MyJsonTask extends AsyncTask<String, JSONArray, Void>
    {
        @Override
        protected void onPreExecute() {
        // TODO Auto-generated method stub
            super.onPreExecute();
        }
        @Override
        protected Void doInBackground(String... params) {
        //Lấy URL truyền vào
            String url=params[0];
            JSONArray jsonObj;

            try {
        //đọc và chuyển về JSONObject
//                JSONArray terminal_array = new JSONArray();
//                JSONArray t_array = terminal_array.put(jsonObject);
        //phai su dung cai json array
                jsonObj = MyJsonReader.readJsonFromUrl(url);
                publishProgress(jsonObj);
            } catch (IOException e) {
        // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
        // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onProgressUpdate(JSONArray... values) {
            super.onProgressUpdate(values);
        //ta cập nhật giao diện ở đây:
            JSONArray jsonObj = values[0];
            try {
        //kiểm tra xem có tồn tại thuộc tính id hay không
//                if(jsonObj.has("id"))
//                    catalogsid.setText(jsonObj.getString("id"));
//                if(jsonObj.has("about"))
//                    catalogname.setText(jsonObj.getString("about"));
//                if(jsonObj.has("quantity"))
//                    quantity.setText(jsonObj.getString("quantity"));
                for(int i = 0; i < jsonObj.length(); i ++) {

                   JSONObject oject = jsonObj.getJSONObject(i);
                   catalogsid.setText(oject.getString("catalogId"));
                   catalogname.setText(oject.getString("catalogName"));
                   quantity.setText(oject.getString("quantity"));
                }

                   // JSONObject object = array.getJSONObject(n);


            } catch (Exception e) {
                Toast.makeText(MainActivity.this, e.toString(),
                        Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
        @Override
        protected void onPostExecute(Void result) {
        // TODO Auto-generated method stub
            super.onPostExecute(result);
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
