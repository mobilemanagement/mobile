package tranduythanh.com;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

public class XemHinhActivity extends Activity {

	ImageView imgView;
	String urlImage="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_xem_hinh);
		imgView=(ImageView) findViewById(R.id.imageView1);
		//Lấy Intent từ MainActivity
		Intent in= getIntent();
		//Lấy link hình ảnh ra được truyền từ MainActivity
		urlImage=in.getStringExtra("URL_IMG");
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//Tiến hành tải hình theo urlImage và hiển thị lên giao diện:
		new ImageLoadTask(urlImage, imgView).execute();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.xem_hinh, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
