package tranduythanh.com;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	TextView txtId;
	TextView txtFirstName;
	TextView txtLastName;
	TextView txtGender;
	TextView txtName;
	TextView txtLocale;
	TextView txtLink;
	TextView txtUserName;
	
	Button btnShowImage;
	
	String urlImage;
	public TextView findTextView(int id)
	{
		return (TextView) findViewById(id);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		addControlAndEvents();
	}
	private void addControlAndEvents() {
		txtId=findTextView(R.id.txtId);
		txtFirstName=findTextView(R.id.txtFirstName);
		txtLastName=findTextView(R.id.txtLastName);
		txtName= findTextView(R.id.txtName);
		txtLink= findTextView(R.id.txtLink);
		txtGender =findTextView(R.id.txtGender);
		txtLocale= findTextView(R.id.txtLocale);
		txtUserName=findTextView(R.id.txtUserName);
		btnShowImage=(Button) findTextView(R.id.btnShowIImage);
//		btnShowImage.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				Intent in=new Intent
//						(MainActivity.this, XemHinhActivity.class);
//				in.putExtra("URL_IMG", urlImage);
//				startActivity(in);
//			}
//		});
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//new MyJsonTask().execute("http://graph.facebook.com/duythanhcse");
		//new MyJsonTask().execute("http://graph.facebook.com/barackobama");
		new MyJsonTask().execute("http://graph.facebook.com/kyduyenhoahau");
	}
	//Lớp xử lý đa tiến trình:
	public class MyJsonTask extends AsyncTask<String, JSONObject, Void>
	{
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		@Override
		protected Void doInBackground(String... params) {
			//Lấy URL truyền vào
			String url=params[0];
			JSONObject jsonObj;
			try {
				//đọc và chuyển về JSONObject
				jsonObj = MyJsonReader.readJsonFromUrl(url);
//				if(jsonObj.has("cover"))
//				{
//					JSONObject objCover= jsonObj.getJSONObject("cover");
//					if(objCover.has("source"))
//					{
//						urlImage=objCover.getString("source");
//					}
//				}
				publishProgress(jsonObj);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onProgressUpdate(JSONObject... values) {
			super.onProgressUpdate(values);
			//ta cập nhật giao diện ở đây:
			JSONObject jsonObj=values[0];
			try {
				//kiểm tra xem có tồn tại thuộc tính id hay không
				if(jsonObj.has("id"))
					txtId.setText(jsonObj.getString("id"));
				if(jsonObj.has("first_name"))
					txtFirstName.setText(jsonObj.getString("first_name"));
				if(jsonObj.has("gender"))
					txtGender.setText(jsonObj.getString("gender"));
				if(jsonObj.has("last_name"))
					txtLastName.setText(jsonObj.getString("last_name"));
				if(jsonObj.has("link"))
					txtLink.setText(jsonObj.getString("link"));
				if(jsonObj.has("locale"))
					txtLocale.setText(jsonObj.getString("locale"));
				if(jsonObj.has("name"))
					txtName.setText(jsonObj.getString("name"));
				if(jsonObj.has("username"))
					txtUserName.setText(jsonObj.getString("username"));
				
				
			} catch (JSONException e) {
				Toast.makeText(MainActivity.this, e.toString(), 
						Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
